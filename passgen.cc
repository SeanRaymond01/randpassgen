#include <iostream>
#include <vector>

std::vector<char> explodeString(std::string string) {

    std::vector<char> temp;

    for (int i = 0; i < string.size(); i++) {

        if (string.at(i) != ' ') {
            temp.push_back(string.at(i));
        }

    }
    
    return temp;

}

std::vector<char> detirmineUserInput(std::string input) {

    std::string lowercaseAlpha = "a b c d e f g h i j k l m n o p q r s t u v w x y z ";
    std::string uppercaseAlpha = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z ";
    std::string numbers = "1 2 3 4 5 6 7 8 9 0 ";
    std::string symbols = "! \" # $ % & ' ( ) *+ , = . / : ; < > ? ? @ [ ]  ^ _ ` { } | ~ ";

    std::string temp;

    if(input.find("lc") != std::string::npos) {
        temp += lowercaseAlpha;
    }
    if(input.find("uc") != std::string::npos) {
        temp += uppercaseAlpha;
    }
    if(input.find("nm") != std::string::npos) {
        temp += numbers;
    }
    if(input.find("sm") != std::string::npos) {
        temp += symbols;
    }

    std::vector<char> tempVec = explodeString(temp); 

    return tempVec;

}



std::string generateString(int length, std::string userInput) {

    std::string temp;

    std::vector<char> useableCharacters = detirmineUserInput(userInput);

    srand(time(NULL));
    for (int i = 0; i < length; i++) {

        std::cout << std::endl << " Generating Random Character..." << std::endl;
        int tempPos = rand() % useableCharacters.size();
        temp += useableCharacters[tempPos];

    }   

    return temp;

}

int main() {

    std::string input;

    std::cout << std::endl << "What character types would you like?" << std::endl << "-lc is lowercase," << std::endl << "-uc is uppercase," << std::endl << "-nm is numbers," << std::endl << "-sm is symbols" << std::endl << "For example, -lcucsm is lowercase, uppercase, and symbols, no numbers." << std::endl;
    std::cin >> input;

    int length;

    std::cout << std::endl << "How long would you like your password to be?" << std::endl;
    std::cin >> length; 

    std::string temp = generateString(length, input);

    std::cout << std::endl << temp;

    return 0;
}