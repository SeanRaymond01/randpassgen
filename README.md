# randpassgen

A crappy random character password generator

This is one of the first projects I completed in C++.

# What is it? # 

randpassgen is a password generator that picks random characters to add to a string that has a user determined length.

## Why should you use it? (not) ## 

* You shouldn't!
    * It sucks!!
    * It isn't secure, safe, or even mildly viable!!
* It doesn't save your passwords!
    * Even if it did, it's plaintext!

### Seriously though, ###

This project was created to entertain myself while I was sitting in a hospital bored.

#### If you want to play with it, here's how ####

When you first run it, it will prompt you what character types you want in the generated string. It will look like the hyphen is required, it isn't. 
I just wanted to feel cool, it really doesn't matter. What does matter however...

* lc means lowercase uppercase
* uc means uppercase letters
* nm means numbers
* sm means symbols

When the program prompts you to what character types you want, your response can be really whatever you want, all it is looking for is those 4 combinations of characters.
If you wanted, you could print the entire script of the bee movie with those character patterns mixed in and it would still work- probably. I haven't tried it and don't want to.

It will then promp you what length you want, this is just the number of characters you want in the string.

It will then pick random characters from the possible character combinations you want.

##### But seriously, please don't use it #####

